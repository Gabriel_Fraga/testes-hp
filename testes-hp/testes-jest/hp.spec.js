const request = require("supertest");
const ApiUrl = "https://healthplay-api.herokuapp.com";

describe("POST /auth/login", () => {
  it("should return 200 and check success'", () => {
        return request(ApiUrl)
        .post("/auth/login")
        .send({email: 'gabriel.fraga.nunes@gmail.com'})
        .send({password: '123456'})
        
        .expect(200)
        .then(() => {

        });
    });
});

let token;

beforeAll((done) => {
  request(ApiUrl)
    .post('/auth/login')
    .send({email: 'teste@teste.com'})
    .send({password: '123456'})
    .end((err, response) => {
      token = response.body.token; // save the token!
      done();
    });
});

describe('GET /home', () => {
  // token not being sent - should respond with a 401
  test('It should require authorization', () => {
    return request(ApiUrl)
      .get('/home')
      .then((response) => {
        expect(response.statusCode).toBe(401);
      });
  });
  // send the token - should respond with a 200
  test('It responds with JSON', () => {
    return request(ApiUrl)
      .get('/home')
      .set('Authorization', `Bearer ${token}`)
      .then((response) => {
        expect.arrayContaining()
        console.log(response.body)
        expect(response.statusCode).toBe(200);
        expect(response.type).toBe('application/json');
      });
  });
});

describe("POST /auth/forgot-password", () => {
    it("should return 200 and check success'", () => {
          return request(ApiUrl)
          .post("/auth/forgot-password")
          .send({email: 'gabriel.fraga.nunes@gmail.com'})
          .expect(204)
          .then(() => {
          });
      });
  });

    describe("POST /auth/register", () => {
    it("should return 200 and check success'", () => {
          return request(ApiUrl)
          .post("/auth/register")
          .send({name: "Gabriel Fraga Novo"})
          .send({email: 'gabriel.fraga.nunes.novo2@gmail.com'})
          .send({password: '1234567'})
          .send({confirmPassword: "1234567"})
          .expect(201)
          .then(() => {
          });
      });
  });

  describe("POST /auth/countries-infos", () => {
    // test('It should require authorization', () => {
    //   return request(ApiUrl)
    //     .get('/home')
    //     .then((response) => {
    //       expect(response.statusCode).toBe(200);
    //     });
    // });
    it("should return 200 and check success'", () => {
      
          return request(ApiUrl)

          .post("/auth/countries-infos")
          .send(token, 'Authorization', `Bearer ${token}`)
          // .send({token: "eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJpZCI6ImhzMkRMR2pXZlQiLCJjdXN0b21lcklkIjoiRUdMTHpVZnpNTiIsIm5hbWUiOiJMdWNhcyBUZXN0ZSIsImlhdCI6MTYxMzc2MTk0MywiZXhwIjoxNjEzNzgzNTQzfQ.lLPVNQbkildaMVNURc_l4pJBKB5y4XKKuflFVzicnB8"})
          .send({custumerId: "EGLLzUfzMN"})
          .send({planId: "gOnFVMXnp9"})
          .send({cep: "99999999"})
          .send({uf: "RS"})
          .send({city: "Porto Alegre"})
          .send({neighborhood: "Bairro A"})
          .send({street: "Rua A"})
          .send({number: "99"})
          .send({complement: "Complemento A"})
          .send({name: "Supidero Man"})
          .send({cpf: "99999999999"})
          .send({cardNumber: "999999999999999999999"})
          .send({validity: "31/12/2030"})
          .send({cvv: "999"})
          // .expect(200)
          .then((response) => {
            expect(response.statusCode).toBe(200);
            expect(response.type).toBe('application/json');
          });
      });
  });
   //   tem que pegar o token

  describe("GET /plans", () => {
      it("should return 200 and check plans'", () => {
            return request(ApiUrl)
            .get("/plans")
            .expect(200)
            .then(response => {
              expect.arrayContaining()
              console.log(response.body);
              for (var i = 0; i < response.body.length; i++) {
                  expect(response.body[i].name).toBeDefined();
                  expect(response.body[i].description).toBeDefined();
                  expect(response.body[i].period).toMatch(/^(Trimestral|Semestral|Anual).*/);
                  // expect(response.body[i].isBestOption).(false || true);
                  if (response.body[i].isBestOption == true){
                      return console.log('true');
                  } else {
                      return console.log('false');
                  }
                  expect(response.body[i].price).toBeDefined();
                  expect(response.body[i].price).not.toBeNaN();
                  expect(response.body[i].objectId).toBeDefined(); 
               }   
            });
        });
    });

  describe("GET /artigos", () => {
      it("should return 200 and check artigos'", () => {
            return request(ApiUrl)
            .get("/artigos")
            .expect(200)
            .then(response => {
              expect.arrayContaining()
              console.log(response.body);
              for (var i = 0; i < response.body.length; i++) {
                  expect(response.body[i].id).toBeDefined();
                  expect(response.body[i].name).toBeDefined();     
                  expect(response.body[i].short_description).toBeDefined();
                  expect(response.body[i].rate).toBeDefined();
                  expect(response.body[i].count_rates).toBeDefined(); 
                  expect(response.body[i].duration).toBeDefined(); 
                  expect(response.body[i].image).toMatch(/^(jpeg||jpg||png).*/);
                  expect(response.body[i].image).toContain('https://parsefiles.back4app.com/');
                  // expect(response.body[i].teacher.)
               }   
            });
        });
    }); // está incompleto, falta acessar o teacher

      describe("GET /artigos/agua-x-cafe-qual-o-melhor", () => {
        it("should return 200 and check artigos", () => {
              return request(ApiUrl)
              .get("/artigos/agua-x-cafe-qual-o-melhor")
              .expect(200)
              .then(response => {
                expect.arrayContaining()
                console.log(response.body);
                for (var i = 0; i < response.body.length; i++) {
                    expect(response.body[i].id).toBeDefined();
                    expect(response.body[i].name).toBeDefined();     
                    expect(response.body[i].short_description).toBeDefined();
                    expect(response.body[i].long_description).toBeDefined();
                    expect(response.body[i].rate).toBeDefined();
                    expect(response.body[i].count_rates).toBeDefined(); 
                    expect(response.body[i].duration).toBeDefined(); 
                    expect(response.body[i].image).toMatch(/^(jpeg||jpg||png).*/);
                    expect(response.body[i].image).toContain('https://parsefiles.back4app.com/');
                 }   
              });
          });
      });

      describe("GET /aulas", () => {
        it("should return 200 and check aulas'", () => {
              return request(ApiUrl)
              .get("/aulas")
              .expect(200)
              .then(response => {
                expect.arrayContaining()
                console.log(response.body);
                for (var i = 0; i < response.body.length; i++) {
                    expect(response.body[i].id).toBeDefined();
                    expect(response.body[i].name).toBeDefined();     
                    expect(response.body[i].short_description).toBeDefined();
                    expect(response.body[i].rate).toBeDefined();
                    expect(response.body[i].count_rates).toBeDefined(); 
                    expect(response.body[i].duration).toBeDefined(); 
                    expect(response.body[i].image).toMatch(/^(jpeg||jpg||png).*/);
                    expect(response.body[i].image).toContain('https://parsefiles.back4app.com/');
                 }   
              });
          });
      }); //   está incompleto, falta acessar o teacher

      describe("GET /aulas/fisioterapia", () => {
        it("should return 200 and check artigos'", () => {
              return request(ApiUrl)
              .get("/aulas/fisioterapia")
              .expect(200)
              .then(response => {
                expect.arrayContaining()
                console.log(response.body);
                for (var i = 0; i < response.body.length; i++) {
                    expect(response.body[i].id).toBeDefined();
                    expect(response.body[i].name).toBeDefined();     
                    expect(response.body[i].short_description).toBeDefined();
                    expect(response.body[i].rate).toBeDefined();
                    expect(response.body[i].count_rates).toBeDefined(); 
                    expect(response.body[i].duration).toBeDefined(); 
                    expect(response.body[i].image).toMatch(/^(jpeg||jpg||png).*/);
                    expect(response.body[i].image).toContain('https://parsefiles.back4app.com/');
                 }   
              });
          });
      }); //   está incompleto, falta acessar o teacher

  
      describe("GET /categories", () => {
        it("should return 200 and check categories'", () => {
              return request(ApiUrl)
              .get("/categories")
              .expect(200)
              .then(response => {
                expect.arrayContaining()
                console.log(response.body);
                for (var i = 0; i < response.body.length; i++) {
                    expect(response.body[i].id).toBeDefined();
                    expect(response.body[i].name).toBeDefined();     
                 }   
              });
          });
      });

      describe("GET /categories/all", () => {
        it("should return 200 and check categories'", () => {
              return request(ApiUrl)
              .get("/categories/all")
              .expect(200)
              .then(response => {
                expect.arrayContaining()
                console.log(JSON.stringify(response.body));
                // console.log(response.body[0].subCategories[0]);
                for (var i = 0; i < response.body.length; i++) {
                    expect(response.body[i].id).toBeDefined();
                    expect(response.body[i].name).toBeDefined();    
                    for (var j = 0; j < response.body[i].subCategories.length; j++) { 
                      expect(response.body[i].subCategories[j].id).toBeDefined();     
                      expect(response.body[i].subCategories[j].name).toBeDefined();     
                    }
                 }   
              });
          });
      });

      describe("GET /aulas/banners", () => {
        it("should return 200 and check banners'", () => {
              return request(ApiUrl)
              .get("/aulas/banners")
              .expect(200)
              .then(response => {
                expect.arrayContaining()
                console.log(response.body);
                for (var i = 0; i < response.body.length; i++) {
                  expect(response.body[i]).toMatch(/^(jpeg||jpg||png).*/);
                  expect(response.body[i]).toContain('https://parsefiles.back4app.com/');
                 }   
              });
          });
      });
    
      describe("GET /artigos/banners", () => {
        it("should return 200 and check banners'", () => {
              return request(ApiUrl)
              .get("/artigos/banners")
              .expect(200)
              .then(response => {
                expect.arrayContaining()
                console.log(response.body);
                for (var i = 0; i < response.body.length; i++) {
                  expect(response.body[i]).toMatch(/^(jpeg||jpg||png).*/);
                  expect(response.body[i]).toContain('https://parsefiles.back4app.com/');
                 }   
              });
          });
      });

        describe("GET /home/search?search=a", () => {
        it("should return 200 and check search'", () => {
              return request(ApiUrl)
              .get("/home/search?search=a")
              .expect(200)
              .then(response => {
                expect.arrayContaining()
                console.log(response.body);
                for (var i = 0; i < response.body.length; i++) {
                  expect(response.body[i].type).toBeDefined();
                  expect(response.body[i].id).toBeDefined();     
                  expect(response.body[i].slug).toBeDefined();
                  expect(response.body[i].name).toBeDefined();
                  expect(response.body[i].image).toMatch(/^(jpeg||jpg||png).*/);
                  expect(response.body[i].image).toContain('https://parsefiles.back4app.com/');
                  expect(response.body[i].duration).toBeDefined(); 
                 }   
              });
          });
      });

       
      describe("GET /artigos/em-alta", () => {
        it("should return 200 and check categories'", () => {
              return request(ApiUrl)
              .get("/artigos/em-alta")
              .expect(200)
              .then(response => {
                expect.arrayContaining()
                console.log(JSON.stringify(response.body));
                // console.log(response.body[0].subCategories[0]);
                for (var i = 0; i < response.body.length; i++) {
                    expect(response.body[i].name).toBeDefined();    
                    for (var j = 0; j < response.body[i].courses.length; j++) { 
                      expect(response.body[i].courses[j].id).toBeDefined();  
                      expect(response.body[i].courses[j].slug).toBeDefined();        
                      expect(response.body[i].courses[j].name).toBeDefined(); 
                      expect(response.body[i].courses[j].short_description).toBeDefined(); 
                      expect(response.body[i].courses[j].rate).toBeDefined();    
                      expect(response.body[i].courses[j].count_rates).toBeDefined();     
                      expect(response.body[i].courses[j].duration).toBeDefined();    
                      expect(response.body[i].courses[j].image).toMatch(/^(jpeg||jpg||png).*/); 
                      expect(response.body[i].courses[j].image).toContain('https://parsefiles.back4app.com/');
                      expect(response.body[i].courses[j].category).toBeDefined();   
                      for (var t = 0; t < response.body[i].courses.length; t++) {    
                        expect(response.body[i].courses[t].id).toBeDefined();  
                        expect(response.body[i].courses[t].name).toBeDefined(); 
                      }      
                    }
                 }   
              });
          });
      });

      describe("GET /aulas/em-alta", () => {
        it("should return 200 and check categories'", () => {
              return request(ApiUrl)
              .get("/aulas/em-alta")
              .expect(200)
              .then(response => {
                expect.arrayContaining()
                console.log(JSON.stringify(response.body));
                // console.log(response.body[0].subCategories[0]);
                for (var i = 0; i < response.body.length; i++) {
                    expect(response.body[i].name).toBeDefined();    
                    for (var j = 0; j < response.body[i].courses.length; j++) { 
                      expect(response.body[i].courses[j].id).toBeDefined();  
                      expect(response.body[i].courses[j].slug).toBeDefined();        
                      expect(response.body[i].courses[j].name).toBeDefined(); 
                      expect(response.body[i].courses[j].short_description).toBeDefined(); 
                        //  if (response.body[i].rate == true){
                        //     return console.log('definido');
                        //   } else {
                        //       return console.log('indefinido');
                        //   }
                      expect(response.body[i].courses[j].rate).toBeDefined();    
                      expect(response.body[i].courses[j].count_rates).toBeDefined();     
                      expect(response.body[i].courses[j].duration).toBeDefined();    
                      expect(response.body[i].courses[j].image).toMatch(/^(jpeg||jpg||png).*/); 
                      expect(response.body[i].courses[j].image).toContain('https://parsefiles.back4app.com/');
                      expect(response.body[i].courses[j].category).toBeDefined();   
                      for (var t = 0; t < response.body[i].courses.length; t++) {    
                        expect(response.body[i].courses[t].id).toBeDefined();  
                        expect(response.body[i].courses[t].name).toBeDefined(); 
                      }      
                    }
                 }   
              });  
          });
      });
  
it("should return 404 with 'The countries was not found'", () => {});
