Dado('que eu faça um POST no endpoint register') do
    @body = {
        "name": "cad cucumber",
        "email": "cucumber2@email.com",
        "password": "123123",
        "confirmPassword": "123123"
      }.to_json
      @post_clients = HTTParty.post 'https://healthplay-api.herokuapp.com/auth/register',
      :body => @body,
      :headers => {"Content-Type" => 'application/json'}
  end
  
  Entao('um novo usuário será registrado') do
    puts @post_clients.body
    puts @post_clients.message
    expect(@post_clients.code).to eq 201
  end