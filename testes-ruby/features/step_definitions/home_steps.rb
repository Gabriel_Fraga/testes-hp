
Dado('o endereço da app') do
    @get_app = HTTParty.get 'https://healthplay-api.herokuapp.com/'
  end
  
  Quando('acessar a home') do
     @get_home= HTTParty.get 'https://healthplay-api.herokuapp.com/home'
  end
  
  Entao('o serviço deve retornar com {int}') do |int|
    expect(int).to eq int
  end
  
  Entao('o retorno será todos os dados da home') do
    puts @get_home.body
    puts "Quantidade de conteúdo: "+@get_home.size.to_s
    puts @get_home.message
  end