    #language: pt
    
    Funcionalidade: Consultar a home
    
    @home
    Cenário: Get home
        Dado o endereço da app
        Quando acessar a home
        Entao o serviço deve retornar com 200
        Entao o retorno será todos os dados da home