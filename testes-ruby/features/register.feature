    #language: pt
    
    Funcionalidade: Usuário se cadastrar na plataforma
    
    @Register
    Cenário: Register in platform
        Dado que eu faça um POST no endpoint register
        Entao um novo usuário será registrado